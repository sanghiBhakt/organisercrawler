package com.organiser.crawler.utils;

public class WebUtils {

    private WebUtils() {
    }

    public static final String ORGANISER_BASE_URL = "https://epaper.organiser.org/index.php?edition=Mpage";

    public static final String BASE_URL = "https://idocuments.s3.ap-south-1.amazonaws.com/encyc/4/";

    public static final String JPEG_EXTENSION = ".jpg";

    public static final String PAGE_SLICE = "_slice";

    public static final String CHAPTER_PAGE = "/Mpage_";

    public static final String ORGANISER_PAGE_LIST_CLASS = "owl-item";

    public static final String ORGANISER_HEADER_TAG = "h1";

}
