package com.organiser.crawler.service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class ImageToPdfConverterService {

    public static final String PDF_EXTENSION = ".pdf";

    public static boolean generatePdfFromFiles(String directoryPath, String pdfName, int margin) throws IOException, DocumentException {

        // Get the Files from the Directory
        Stream<Path> walk = Files.walk(Paths.get(directoryPath));
        List<String> imagesInDirectory = walk.filter(Files::isRegularFile)
                .map(x -> x.toString()).collect(Collectors.toList());

        // Sort the Files in the directory and margins
        Collections.sort(imagesInDirectory, new SortFilesByNumberComparator());

        // PDF Document Initialization
        Document pdfDoc = new Document();
        PdfWriter.getInstance(pdfDoc, new FileOutputStream(String.format("%s/%s%s", directoryPath, pdfName, PDF_EXTENSION)));

        // Copy the Files to the PDF File
        try {
            pdfDoc.open();
            for(String imageInDirectory: imagesInDirectory) {
                log.info(String.format("Pdf conversion of File: %s", imageInDirectory));
                Image image = Image.getInstance(imageInDirectory);
                image.scaleToFit(image.getScaledWidth(), image.getScaledHeight());
                pdfDoc.setMargins(margin, margin, margin, margin);
                pdfDoc.setPageSize(new Rectangle(image.getScaledWidth() + 2 * margin,
                        image.getScaledHeight() + 2 * margin));
                pdfDoc.newPage();
                pdfDoc.add(image);
            }
            pdfDoc.close();
        } catch (Exception ex) {
            return false;
        }
        return true;

    }

}
