package com.organiser.crawler.service;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SortFilesByNumberComparator implements Comparator<String> {

    @Override
    public int compare(String str1, String str2) {
        Pattern p = Pattern.compile("(\\d+)(?!.*\\d)");
        Matcher m1 = p.matcher(str1);
        int num1 = 0, num2 = 0;
        while(m1.find()) {
            num1 = Integer.parseInt(m1.group());
        }
        Matcher m2 = p.matcher(str2);
        while(m2.find()) {
            num2 = Integer.parseInt(m2.group());
        }
        return num1 - num2;
    }

}
