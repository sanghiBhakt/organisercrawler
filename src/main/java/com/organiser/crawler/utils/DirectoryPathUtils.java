package com.organiser.crawler.utils;

public class DirectoryPathUtils {

    public final static String PARENT_DIRECTORY = "Organiser";

    public static final String JPEG_EXTENSION = ".jpg";

    public static final String PDF_EXTENSION = ".pdf";

}
