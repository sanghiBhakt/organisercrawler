package com.organiser.crawler.commands;

import com.itextpdf.text.DocumentException;
import com.organiser.crawler.service.ImageSliceToPdfConverterService;
import com.organiser.crawler.service.ImageToPdfConverterService;
import com.organiser.crawler.utils.DirectoryPathUtils;
import com.organiser.crawler.utils.WebUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@ShellComponent
@Slf4j
public class OrganiserCrawler {

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    public static Calendar calendar = Calendar.getInstance();

    public static String startDate = "2014/03/29";

    public static String midDate = "2015/04/18";

    public static final int margin = 0;

    @ShellMethod(key="org-get-all", value="Get all issues of the Organiser!")
    public void getAllIssuesOfOrganiser() throws ParseException, MalformedURLException {
        File parentDirectory = new File(DirectoryPathUtils.PARENT_DIRECTORY);
        if(parentDirectory.exists() || parentDirectory.mkdirs()) {
            Date date = sdf.parse(startDate);
            while(date.compareTo(sdf.parse(midDate)) < 0) {
                date = getIssueWithSingleSlicePage(date);
            }
            while(date.compareTo(sdf.parse(sdf.format(new Date()))) < 0) {
                date = getIssueWithMultipleSlicePage(date);
            }
        } else {
            log.error("IOException: Couldn't Create Parent Directory:(");
        }
        log.info("Succesfully downloaded all Issue of Organiser!");
        return;
    }

    @ShellMethod(key="pdf", value="Generate Pdf for Image Slices")
    public void getPdfForImageSlices(@ShellOption({"-D", "--date"}) String issueDate) throws ParseException, IOException, DocumentException {
        Date date = sdf.parse(issueDate);
        String currDate = sdf.format(date);
        currDate = currDate.replaceAll("/", "-");

        new ImageSliceToPdfConverterService().generatePdfFromFiles(String.format("%s/%s",
                DirectoryPathUtils.PARENT_DIRECTORY, currDate), currDate, 0);
        File file = new File(String.format("%s/%s/%s%s", DirectoryPathUtils.PARENT_DIRECTORY,
                currDate, currDate, DirectoryPathUtils.PDF_EXTENSION));
        if(file.exists()) {
            log.info("Successfully generated the PDF for the Issue!");
        } else {
            log.error("Couldn't generate the PDF for the Issue :(");
        }
        return;
    }

    @ShellMethod(key="org-get", value="Get issue of specific date!")
    public void getParticularIssueOfOrganiser(@ShellOption({"-D", "--date"}) String issueDate) throws ParseException, MalformedURLException {
        File parentDirectory = new File(DirectoryPathUtils.PARENT_DIRECTORY);
        if(parentDirectory.exists() || parentDirectory.mkdirs()) {
            Date date = sdf.parse(issueDate);
            if((date.compareTo(sdf.parse(startDate)) >= 0) && (date.compareTo(sdf.parse(midDate)) < 0)) {
                getIssueWithSingleSlicePage(date);
            } else if((date.compareTo(sdf.parse(midDate)) >= 0) && (date.compareTo(sdf.parse(sdf.format(new Date()))) <= 0)) {
                getIssueWithMultipleSlicePage(date);
            } else {
                log.error("Entered Issue date is Invalid!");
            }

            String currDate = sdf.format(date);
            currDate = currDate.replaceAll("/", "-");
            File file = new File(String.format("%s/%s/%s%s", DirectoryPathUtils.PARENT_DIRECTORY,
                    currDate, currDate, DirectoryPathUtils.PDF_EXTENSION));
            if(file.exists()) {
                log.info("Successfully downloaded the queries issue of Organiser");
            } else {
                log.error("Couldn't download the issue because of server error or invalid date");
            }
        } else {
            log.error("IOException: Couldn't Create Parent Directory:(");
        }
        return;
    }

    public Date getIssueWithSingleSlicePage(Date date) throws ParseException, MalformedURLException {
        calendar.setTime(date);
        String currDate = sdf.format(date);
        currDate = currDate.replaceAll("/", "-");
        File issueDirectory = new File(String.format("%s/%s", DirectoryPathUtils.PARENT_DIRECTORY, currDate));
        if(issueDirectory.mkdirs()) {
            int pageCounter = 1;
            byte[] byteArray =  new byte[1024];
            int bytesLength;
            try {
                // Set URL for the Issue's first page
                URL pageUrl = new URL(String.format("%s%s%s%s%s", WebUtils.BASE_URL, sdf.format(date),
                        WebUtils.CHAPTER_PAGE, String.valueOf(pageCounter), WebUtils.JPEG_EXTENSION));
                log.info(String.valueOf(pageUrl));
                HttpURLConnection imageUrl = (HttpURLConnection) pageUrl.openConnection();

                // Run until the end of Issue's last page
                while(imageUrl.getResponseCode() != 403) {

                    // Write the image bytes to the image
                    FileOutputStream fos = new FileOutputStream(String.format("%s/%s/%s%s",
                            DirectoryPathUtils.PARENT_DIRECTORY, currDate, pageCounter, DirectoryPathUtils.JPEG_EXTENSION));
                    try {
                        InputStream inputStream = pageUrl.openStream();
                        while((bytesLength = inputStream.read(byteArray)) != -1) {
                            fos.write(byteArray, 0, bytesLength);
                        }
                        log.info("Successfully Downloaded Page!");
                        fos.flush();
                        fos.close();
                        inputStream.close();
                    } catch (Exception ex) {
                        log.error("Exception: Couldn't Download Page :(");
                    }

                    // Update the pageUrl, imageUrl and pageCounter
                    pageCounter += 1;
                    pageUrl = new URL(String.format("%s%s%s%s%s", WebUtils.BASE_URL, sdf.format(date),
                            WebUtils.CHAPTER_PAGE, String.valueOf(pageCounter), WebUtils.JPEG_EXTENSION));
                    log.info(String.valueOf(pageUrl));
                    imageUrl = (HttpURLConnection) pageUrl.openConnection();

                }

                // Convert Image to PDF File
                if(new ImageToPdfConverterService().generatePdfFromFiles(String.format("%s/%s",
                        DirectoryPathUtils.PARENT_DIRECTORY, currDate), currDate, 0)) {
                    log.info("Successfully downloaded Issue!");
                } else {
                    log.error("Error downloading Issue!");
                }
            } catch (Exception ex) {
                log.error("Exception: Issue with I/O or Issue with downloading page :(");
            }

        } else {
            log.error("IOException: Couldn't Create Issue Directory:(");
        }

        // Update the date to reflect the next Issue
        calendar.add(Calendar.DAY_OF_MONTH, 7);
        return calendar.getTime();
    }

    public Date getIssueWithMultipleSlicePage(Date date) {
        calendar.setTime(date);
        String currDate = sdf.format(date);
        currDate = currDate.replaceAll("/", "-");
        File issueDirectory = new File(String.format("%s/%s", DirectoryPathUtils.PARENT_DIRECTORY, currDate));
        if(issueDirectory.mkdirs()) {
            int pageCounter = 1;
            int sliceCounter = 1;
            byte[] byteArray =  new byte[1024];
            int bytesLength;
            try {
                // Set URL for the Issue's first page
                URL pageUrl = new URL(String.format("%s%s%s%s%s%s%s", WebUtils.BASE_URL, sdf.format(date),
                        WebUtils.CHAPTER_PAGE, String.valueOf(pageCounter), WebUtils.PAGE_SLICE,
                        String.valueOf(sliceCounter), WebUtils.JPEG_EXTENSION));
                log.info(String.valueOf(pageUrl));
                HttpURLConnection imageUrl = (HttpURLConnection) pageUrl.openConnection();

                // Run until the end of Issue's last page
                while(imageUrl.getResponseCode() != 403) {

                    // Write the image bytes to the image
                    FileOutputStream fos = null;
                    boolean isDownloaded = true;
                    while(sliceCounter <= 14) {
                        try {
                            fos = new FileOutputStream(String.format("%s/%s/%s%02d%s", DirectoryPathUtils.PARENT_DIRECTORY,
                                    currDate, pageCounter, sliceCounter, DirectoryPathUtils.JPEG_EXTENSION), true);
                            InputStream inputStream = pageUrl.openStream();
                            while((bytesLength = inputStream.read()) != -1) {
                                fos.write(bytesLength);
                            }
                            log.info("Successfully Downloaded Slice!");
                            fos.flush();
                            fos.close();
                            inputStream.close();
                        } catch (Exception ex) {
                            isDownloaded = false;
                            log.error("Exception: Couldn't Download Slice :(");
                        }
                        sliceCounter += 1;
                        pageUrl = new URL(String.format("%s%s%s%s%s%s%s", WebUtils.BASE_URL, sdf.format(date),
                                WebUtils.CHAPTER_PAGE, String.valueOf(pageCounter), WebUtils.PAGE_SLICE,
                                String.valueOf(sliceCounter), WebUtils.JPEG_EXTENSION));
                        log.info(String.valueOf(pageUrl));
                        imageUrl = (HttpURLConnection) pageUrl.openConnection();
                    }
                    if(isDownloaded) {
                        log.info("Successfully Downloaded Page!");
                    } else {
                        log.info("Exception: Couldn't Download Page!");
                    }

                    // Update the pageUrl, imageUrl and pageCounter
                    pageCounter += 1;
                    sliceCounter = 1;
                    pageUrl = new URL(String.format("%s%s%s%s%s%s%s", WebUtils.BASE_URL, sdf.format(date),
                            WebUtils.CHAPTER_PAGE, String.valueOf(pageCounter), WebUtils.PAGE_SLICE,
                            String.valueOf(sliceCounter), WebUtils.JPEG_EXTENSION));
                    log.info(String.valueOf(pageUrl));
                    imageUrl = (HttpURLConnection) pageUrl.openConnection();

                }

                // Convert Image to PDF File
                if(new ImageSliceToPdfConverterService().generatePdfFromFiles(String.format("%s/%s",
                        DirectoryPathUtils.PARENT_DIRECTORY, currDate), currDate, 0)) {
                    log.info("Successfully downloaded Issue!");
                } else {
                    log.error("Error downloading Issue!");
                }
            } catch (Exception ex) {
                log.error("Exception: Issue with I/O or Issue with downloading page :(");
            }

        } else {
            log.error("IOException: Couldn't Create Issue Directory:(");
        }

        // Update the date to reflect the next Issue
        calendar.add(Calendar.DAY_OF_MONTH, 7);
        return calendar.getTime();
    }

}
