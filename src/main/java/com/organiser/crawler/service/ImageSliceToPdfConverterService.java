package com.organiser.crawler.service;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class ImageSliceToPdfConverterService {

    public static final String PDF_EXTENSION = ".pdf";

    public static boolean generatePdfFromFiles(String directoryPath, String pdfName, int margin) throws IOException, DocumentException {

        // Get the Files from the Directory
        Stream<Path> walk = Files.walk(Paths.get(directoryPath));
        List<String> imagesInDirectory = walk.filter(Files::isRegularFile)
                .map(x -> x.toString()).collect(Collectors.toList());

        // Sort the Files in the directory and margins
        Collections.sort(imagesInDirectory, new SortFilesByNumberComparator());

        // Copy the Files to the PDF File
        try {
            // PDF Document Initialization
            Document pdfDoc = new Document();
            PdfWriter.getInstance(pdfDoc, new FileOutputStream(String.format("%s/%s%s", directoryPath, pdfName, PDF_EXTENSION)));
            Image firstImage = Image.getInstance(imagesInDirectory.iterator().next());
            pdfDoc.setPageSize(new Rectangle(firstImage.getScaledWidth() + 2 * margin, firstImage.getScaledHeight() * 14 + 2 * margin));
            pdfDoc.setMargins(margin, margin, margin, margin);
            pdfDoc.open();
            int newPageCounter = 1;
            log.info(String.valueOf(pdfDoc.getPageSize()));

            for(String imageInDirectory: imagesInDirectory) {
                if(newPageCounter > 14) {
                    pdfDoc.newPage();
                    newPageCounter = 1;
                }

                log.info(String.format("Pdf conversion of File: %s", imageInDirectory));
                Image image = Image.getInstance(imageInDirectory);
                pdfDoc.setPageSize(new Rectangle(image.getScaledWidth() + 2 * margin,
                        image.getScaledHeight() * 14 + 2 * margin));
                image.scaleToFit(image.getScaledWidth(), image.getScaledHeight());
                pdfDoc.setMargins(margin, margin, margin, margin);
                pdfDoc.add(image);
                newPageCounter += 1;
                log.info(String.valueOf(pdfDoc.getPageSize()));
            }
            pdfDoc.close();
        } catch (Exception ex) {
            return false;
        }
        return true;

    }

}
