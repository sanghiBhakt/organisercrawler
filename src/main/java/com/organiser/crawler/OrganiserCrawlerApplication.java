package com.organiser.crawler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrganiserCrawlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrganiserCrawlerApplication.class, args);
	}

}
